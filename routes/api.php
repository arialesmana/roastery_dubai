<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('v1/login', 'API\AuthController@login');

Route::get('{code}/order/data/{order}', 'APIController@order')->name('katalog.monitoring.order');

Route::group(['middleware' => 'auth:api'], function () {

    Route::get('v1/list-jenis', 'API\JenisController@index');
    Route::get('v1/detail-jenis/{id}', 'API\JenisController@detail');
    Route::get('v1/delete-jenis/{id}', 'API\JenisController@delete');
    Route::post('v1/add-jenis', 'API\JenisController@add');
    Route::post('v1/edit-jenis/{id}', 'API\JenisController@edit');

    Route::get('v1/list-supplier', 'API\SupplierController@index');
    Route::get('v1/detail-supplier/{id}', 'API\SupplierController@detail');
    Route::get('v1/delete-supplier/{id}', 'API\SupplierController@delete');
    Route::post('v1/add-supplier', 'API\SupplierController@add');
    Route::post('v1/edit-supplier/{id}', 'API\SupplierController@edit');

    Route::get('v1/list-batch', 'API\BatchController@index');
    Route::get('v1/detail-batch/{id}', 'API\BatchController@detail');
    Route::get('v1/delete-batch/{id}', 'API\BatchController@delete');
    Route::post('v1/add-batch', 'API\BatchController@add');
    Route::get('v1/add-detail', 'API\BatchController@add_detail');
    Route::post('v1/edit-batch/{id}', 'API\BatchController@edit');

    Route::get('v1/list-product', 'API\ProductController@index');
    Route::get('v1/add-detail-product', 'API\ProductController@add_detail');
    Route::post('v1/add-product', 'API\ProductController@add');
    Route::get('v1/get-sub-category/{id}', 'API\ProductController@sub_category');
    Route::get('v1/detail-product/{id}', 'API\ProductController@detail');
    Route::get('v1/delete-product/{id}', 'API\ProductController@delete');
    Route::post('v1/update-qr-product/{id}', 'API\ProductController@update_qr');
    Route::post('v1/edit-product/{id}', 'API\ProductController@edit');

    Route::post('v1/add-biji', 'API\BijiController@add');
    Route::get('v1/delete-biji/{id}', 'API\BijiController@delete');
    Route::get('v1/detail-biji/{id}', 'API\BijiController@detail');
    Route::post('v1/edit-biji/{id}', 'API\BijiController@edit');

    Route::post('v1/add-unit', 'API\UnitController@add');
    Route::get('v1/delete-unit/{id}', 'API\UnitController@delete');
    Route::get('v1/get-unit-detail', 'API\UnitController@get_unit_detail');
    Route::get('v1/detail-unit/{id}', 'API\UnitController@detail');
    Route::post('v1/edit-unit/{id}', 'API\UnitController@edit');

    Route::post('v1/add-proses', 'API\ProsesController@add');
    Route::get('v1/delete-proses/{id}', 'API\ProsesController@delete');
    Route::get('v1/detail-proses/{id}', 'API\ProsesController@detail');
    Route::post('v1/edit-proses/{id}', 'API\ProsesController@edit');

    //app Roastery
    Route::get('v1/list-master-data', 'API\MasterDataController@index');

    Route::post('v1/add-roasting-profile', 'API\RoastingProfileController@add');
    Route::get('v1/delete-roasting-profile/{id}', 'API\RoastingProfileController@delete');
    Route::get('v1/detail-roasting-profile/{id}', 'API\RoastingProfileController@detail');
    Route::post('v1/edit-roasting-profile/{id}', 'API\RoastingProfileController@edit');

    Route::post('v1/add-whole-bean', 'API\WholeBeanController@add');
    Route::get('v1/delete-whole-bean/{id}', 'API\WholeBeanController@delete');
    Route::get('v1/detail-whole-bean/{id}', 'API\WholeBeanController@detail');
    Route::post('v1/edit-whole-bean/{id}', 'API\WholeBeanController@edit');

    Route::post('v1/add-weight', 'API\WeightController@add');
    Route::get('v1/delete-weight/{id}', 'API\WeightController@delete');

    Route::post('v1/add-roasting', 'API\RoastingController@add');
    Route::get('v1/delete-roasting/{id}', 'API\RoastingController@delete');
    Route::get('v1/list-roasting', 'API\RoastingController@index');
    Route::get('v1/detail-roasting/{id}', 'API\RoastingController@detail');
    Route::post('v1/edit-roasting/{id}', 'API\RoastingController@edit');


    // app pabrik
    Route::get('v1/list-production', 'API\ProductionController@index');
    Route::post('v1/add-production', 'API\ProductionController@add');
    Route::get('v1/summary-production', 'API\ProductionController@summary');

    Route::get('v1/list-logistic', 'API\LogisticController@index');
    Route::post('v1/add-logistic', 'API\LogisticController@add');
    Route::get('v1/summary-logistic', 'API\LogisticController@summary');

    Route::get('v1/sales', 'API\SalesController@index');
    Route::post('v1/add-sales', 'API\SalesController@add');
    Route::get('v1/summary-sales', 'API\SalesController@summary');
    // end app pabrik
});
