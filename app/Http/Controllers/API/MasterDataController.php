<?php

namespace App\Http\Controllers\API;

use App\Biji;
use App\Http\Controllers\Controller;
use App\Jenis;
use App\Proses;
use Illuminate\Http\Request;
use Auth;
use App\RoastingProfile;
use App\Supplier;
use App\Unit;
use App\WholeBean;
use DB;

class MasterDataController extends Controller
{
    public $successStatus = 401;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['roasting_profile']   = RoastingProfile::all();
        $success['whole_bean']   = WholeBean::all();
        $success['unit']   = Unit::all();
        $success['biji']   = Biji::all();
        $success['jenis']   = Jenis::all();
        $success['proses']   = Proses::all();
        $success['supplier']   = Supplier::all();

        return response()->json($success, $this->successStatus);
    }
}
