<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Product\ProductVendor;
use App\Product\Product;
use App\Product\ProductGroup;
use App\Product\ProductGroupRule;
use App\Vendor\VendorContractMaster;
use App\Product\ProductContract;
use App\Category\CategoryProduct;
use App\Vendor\VendorMaster;
use App\Category\Category;
use App\Batch;
use DB;

class ProductController extends Controller
{
    public $successStatus = 401;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        // DB::setDefaultConnection($this->user->group->katalog);
        // $product = ProductVendor::with(['product'])
        //     ->has('product')
        //     ->whereHas('product', function ($p) {
        //         $p->where('status', 1);
        //     })
        //     ->where('vendor_id', $this->user->vendor_katalog_id)->get();

        // $this->successStatus = 200;
        // $success['success'] = true;
        // $success['product'] = $product;

        // return response()->json($success, $this->successStatus);
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['product']   = Product::all();

        return response()->json($success, $this->successStatus);
    }

    public function detail(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['product']   = Product::find($id);

        return response()->json($success, $this->successStatus);
    }

    public function update_qr(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        if (Product::where('id', $id)->exists()) {
            $product = Product::find($id);

            if ($request->hasFile('files_qr')) {
                $files          = $request->file('files_qr');
                $files->move(public_path("images/product/") . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $product->id, $request->fileName_qr);
                $product->qrcode  = "images/product/" . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $product->id . '/' . $request->fileName_qr;
                $product->save();
            }

            return response()->json([
                "message" => "records updated successfully"
            ], 200);
        } else {
            return response()->json([
                "message" => "Product not found"
            ], 404);
        }
    }

    public function delete(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $product = Product::find($id);
        $product->delete();
        $this->successStatus = 200;
        $success['success'] = true;
        return response()->json($success, $this->successStatus);
    }

    public function add_detail(Request $request)
    {
        DB::setDefaultConnection('mysql');
        $cat = VendorContractMaster::select('category')->where('vendor_master_id', Auth::user()->id)->get()->pluck('category');

        $category = new Category;
        $category = $category->setConnection($this->user->group->katalog);
        $success['category'] = $category->whereIn('id', $cat)->get();

        $product_group_rules = new ProductGroupRule;
        $product_group_rules = $product_group_rules->setConnection($this->user->group->katalog);
        $success['product_group_rules'] = $product_group_rules->get();

        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $success['batch']   = Batch::orderBy('created_at', 'desc')->get();

        // $batch = new Batch;
        // $batch = $batch->setConnection($this->user->group->katalog);
        // $success['batch'] = $batch->get();

        $this->successStatus = 200;
        $success['success'] = true;

        return response()->json($success, $this->successStatus);
    }

    public function sub_category(Request $request, $id)
    {
        $category = new Category;
        $category = $category->setConnection($this->user->group->katalog);
        $success['subcategory'] = $category->where('level', 3)->where('parent_id', $id)->get();

        $this->successStatus = 200;
        $success['success'] = true;

        return response()->json($success, $this->successStatus);
    }

    public function add(Request $request)
    {
        $product = new Product;
        $product = $product->setConnection($this->user->group->katalog);
        $product                        = new Product;
        $product                        = $product->setConnection($this->user->group->katalog);
        $product->productID             = $request->productID;
        $product->name                  = $request->name;
        $product->biji_id               = $request->biji_id;
        $product->jenis_id              = $request->jenis_id;
        $product->proses_id             = $request->proses_id;
        $product->supplier_id           = $request->supplier_id;
        
        $product->save();

        $this->successStatus = 200;
        $success['success']  = true;
        $success['product']     = $product;
        $batch = new Batch;
        $batch = $batch->setConnection($this->user->group->katalog);
        $success['batch']    = $batch->find($product->batch_id);

        return response()->json($success, $this->successStatus);
    }

    public function edit(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        if (Product::where('id', $id)->exists()) {
            $product = Product::find($id);
            if ($request->has('productID')) {
                $product->productID      = $request->productID;
            }
            if ($request->has('name')) {
                $product->name = $request->name;
            }
            if ($request->has('biji_id')) {
                $product->biji_id = $request->biji_id;
            }
            if ($request->has('jenis_id')) {
                $product->jenis_id = $request->jenis_id;
            }
            if ($request->has('proses_id')) {
                $product->proses_id = $request->proses_id;
            }
            if ($request->has('supplier_id')) {
                $product->supplier_id = $request->supplier_id;
            }
            $product->save();

            return response()->json([
                "message" => "records updated successfully"
            ], 200);
        } else {
            return response()->json([
                "message" => "Product not found"
            ], 404);
        }
    }
}
