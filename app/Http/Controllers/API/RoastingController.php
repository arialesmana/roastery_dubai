<?php

namespace App\Http\Controllers\API;

use App\BlendedBeans;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Inventory;
use App\Roasting;
use DB;

class RoastingController extends Controller
{
    public $successStatus = 401;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['roasting']   = Roasting::all();
        $success['inventory']   = Inventory::all();

        return response()->json($success, $this->successStatus);
    }

    public function detail(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['roasting']   = Roasting::find($id);
        $success['blendedBeans']   = BlendedBeans::where('roasting_id', $id)->get();

        return response()->json($success, $this->successStatus);
    }

    public function delete(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $roasting = Roasting::find($id);
        $roasting->delete();
        BlendedBeans::where('roasting_id', $id)->delete();

        $this->successStatus = 200;
        $success['success'] = true;
        return response()->json($success, $this->successStatus);
    }

    public function add(Request $request)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        if ($request->roastingMenu == "Single") {
            $roasting = new Roasting();
            $roasting = $roasting->setConnection($this->user->group->katalog);
            
            $roasting->roasting_name            = $request->roasting_name;
            $roasting->roasting_menu            = $request->roastingMenu;
            $roasting->roasting_profile_id      = $request->roasting_profile_id;
            $roasting->bean_id                  = $request->bean_id;
            $roasting->flavor_note              = $request->flavor_note;
            $roasting->whole_bean_id            = $request->whole_bean_id;
            $roasting->save();

            if(Inventory::where('bean_id', $request->bean_id)->first())
            {
                $inventory = Inventory::where('bean_id', $request->bean_id)->first();
                $inventory->weight              = $inventory->weight + $request->weight;
                $inventory->save();
            }
            else
            {
                $inventory = new Inventory();
                $inventory = $inventory->setConnection($this->user->group->katalog);
                $inventory->bean_id                  = $request->bean_id;
                $inventory->weight                   = $request->weight;
                $inventory->unit                     = $request->unit;
                $inventory->save();
            }
        }

        else if ($request->roastingMenu == "Blended") {
            
            if ($request->has('beans')) {
                $products = json_decode($request->beans);

                $roasting = new Roasting();
                $roasting = $roasting->setConnection($this->user->group->katalog);
                $roasting->roasting_name            = $request->roasting_name;
                $roasting->roasting_menu            = $request->roastingMenu;
                $roasting->roasting_profile_id      = $request->roasting_profile_id;
                // $roasting->bean_id                  = $product->selectBean;
                $roasting->flavor_note              = $request->flavor_note;
                $roasting->whole_bean_id            = $request->whole_bean_id;
                $roasting->save();

                foreach ($products as $product)
                {
                    $blendedBeans = new BlendedBeans();
                    $blendedBeans-> bean_id         = $product->selectBean;
                    $blendedBeans-> roasting_id     = $roasting->id;
                    $blendedBeans-> persentase      = $product->persentase;
                    $blendedBeans->save();

                    if(Inventory::where('bean_id', $product->selectBean)->first())
                    {
                        $inventory = Inventory::where('bean_id', $product->selectBean)->first();
                        $inventory->weight                   = $inventory->weight + ($request->weight * $product->persentase / 100);
                        $inventory->save();
                    }
                    else
                    {
                        $inventory = new Inventory();
                        $inventory = $inventory->setConnection($this->user->group->katalog);
                        $inventory->bean_id                  = $product->selectBean;
                        $inventory->weight                   = $request->weight * $product->persentase / 100;
                        $inventory->unit                     = $request->unit;
                        $inventory->save();
                    }
                }
            } 
        }

        $this->successStatus = 200;
        $success['success']  = true;
        // $success['data']     = $roasting;

        return response()->json($success, $this->successStatus);
    }

    public function edit(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        if (Roasting::where('id', $id)->exists()) {
            $roasting = Roasting::find($id);
            if ($request->has('roasting_name')) {
                $roasting->roasting_name      = $request->roasting_name;
            }
            if ($request->has('roasting_profile_id')) {
                $roasting->roasting_profile_id      = $request->roasting_profile_id;
            }
            if ($request->has('flavor_note')) {
                $roasting->flavor_note = $request->flavor_note;
            }
            if ($request->has('whole_bean_id')) {
                $roasting->whole_bean_id = $request->whole_bean_id;
            }
            // if ($request->has('weight')) {
            //     $roasting->weight = $request->weight;
            // }
            // if ($request->has('unit')) {
            //     $roasting->unit = $request->unit;
            // }
            if ($request->has('bean_id')) {
                $roasting->bean_id = $request->bean_id;
            }
            if ($request->has('beans')) {

                $products = json_decode($request->beans);
                $blendedBeans = BlendedBeans::where('roasting_id', $id)->get();

                foreach ($products as $product)
                {
                    foreach ($blendedBeans as $blendedBean)
                    {
                        $blendedBean-> bean_id         = $product->selectBean;
                        $blendedBean-> roasting_id     = $roasting->id;
                        $blendedBean-> persentase      = $product->persentase;
                        $blendedBean->save();
                    }
                }
            } 
            $roasting->save();

            return response()->json([
                "message" => "records updated successfully"
            ], 200);
        } else {
            return response()->json([
                "message" => "Product not found"
            ], 404);
        }
    }
}
